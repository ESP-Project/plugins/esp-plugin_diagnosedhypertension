'''
                                  ESP Health
                         Notifiable Diseases Framework
                            Diagnosed Hypertension   


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2019 Commonwealth Informatics, Inc.
@license: LGPL
'''

import datetime, sys
from collections import defaultdict, OrderedDict
from dateutil.relativedelta import relativedelta
from ESP.utils import log
from ESP.utils.utils import queryset_iterator
from ESP.settings import DATABASES
from django.db import IntegrityError, connection
from django.db.models import F,Max,Q,Min
from django.contrib.contenttypes.models import ContentType

from ESP.emr.models import Encounter, Patient
from ESP.hef.models import Event
from ESP.hef.base import PrescriptionHeuristic,BaseEventHeuristic
from ESP.hef.base import DiagnosisHeuristic, Dx_CodeQuery, HEF_CORE_URI
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.static.models import DrugSynonym

from diag_bp_utils import generate_diagnosed_hypertension_cases

class DiagnosedHypertension(DiseaseDefinition):
    '''
    DiagnosedHypertension
    '''
    
    conditions = ['diagnosedhypertension']
    
    uri = 'urn:x-esphealth:disease:commoninf:diagnosedhypertension:sql'
    
    short_name = 'diagnosedhypertension'
    
    test_name_search_strings = [
        
        ]
    
    timespan_heuristics = []

    
    def update_drugsyn(self):
        druglist=(('hydrochlorothiazide','hydrochlorothiazide','self'),
                  ('hydrochlorothiazide','hctz',None),
                  ('hydrochlorothiazide','esidrix',None),
                  ('hydrochlorothiazide','oretic',None),
                  ('hydrochlorothiazide','microzide',None),
                  ('hydrochlorothiazide','hydrodiuril',None),
                  ('chlorthalidone','chlorthalidone','self'),
                  ('chlorthalidone','thalitone',None),
                  ('indapamide','indapamide','self'),
                  ('indapamide','lozol',None),
                  ('amlodipine','amlodipine','self'),
                  ('amlodipine','norvasc',None),
                  ('clevidipine','clevidipine','self'),
                  ('clevidipine','cleviprex',None),
                  ('felodipine','felodipine','self'),
                  ('felodipine','plendil',None),
                  ('isradipine','isradipine','self'),
                  ('isradipine','dynacirc',None),
                  ('nicardipine','nicardipine','self'),
                  ('nicardipine','cardene',None),
                  ('nifedipine','nifedipine','self'),
                  ('nifedipine','procardia',None),
                  ('nifedipine','adalat',None),
                  ('nisoldipine','nisoldipine','self'),
                  ('nisoldipine','sular',None),
                  ('diltiazem','diltiazem','self'),
                  ('diltiazem','cardizem',None),
                  ('diltiazem','cartia',None),
                  ('diltiazem','diltia',None),
                  ('diltiazem','diltzac',None),
                  ('diltiazem','tiazac',None),
                  ('diltiazem','taztia',None),
                  ('verapamil','verapamil','self'),
                  ('verapamil','isoptin',None),
                  ('verapamil','calan',None),
                  ('verapamil','covera',None),
                  ('verapamil','verelan',None),
                  ('acebutolol','acebutolol','self'),
                  ('acebutolol','sectral',None),
                  ('atenolol','atenolol','self'),
                  ('atenolol','tenormin',None),
                  ('betaxolol','betaxolol','self'),
                  ('betaxolol','kerlone',None),
                  ('bisoprolol','bisoprolol','self'),
                  ('bisoprolol','zebeta',None),
                  ('carvedilol','carvedilol','self'),
                  ('carvedilol','coreg',None),
                  ('labetolol','labetolol','self'),
                  ('labetolol','trandate',None),
                  ('metoprolol','metoprolol','self'),
                  ('metoprolol','lopressor',None),
                  ('nadolol','nadolol','self'),
                  ('nadolol','corgard',None),
                  ('nebivolol','nebivolol','self'),
                  ('nebivolol','bystolic',None),
                  ('pindolol','pindolol','self'),
                  ('pindolol','visken',None),
                  ('propranolol','propranolol','self'),
                  ('propranolol','inderal',None),
                  ('benazepril','benazepril','self'),
                  ('benazepril','lotensin',None),
                  ('catopril','catopril','self'),
                  ('catopril','capoten',None),
                  ('enalapril','enalapril','self'),
                  ('enalapril','enalaprilat',None),
                  ('enalapril','vasotec',None),
                  ('fosinopril','fosinopril','self'),
                  ('fosinopril','monopril',None),
                  ('lisinopril','lisinopril','self'),
                  ('lisinopril','prinivil',None),
                  ('lisinopril','zestril',None),
                  ('moexipril','moexipril','self'),
                  ('moexipril','univasc',None),
                  ('perindopril','perindopril','self'),
                  ('perindopril','aceon',None),
                  ('quinapril','quinapril','self'),
                  ('quinapril','accupril',None),
                  ('ramipril','ramipril','self'),
                  ('ramipril','altace',None),
                  ('trandolapril','trandolapril','self'),
                  ('trandolapril','mavik',None),
                  ('candesartan','candesartan','self'),
                  ('candesartan','atacand',None),
                  ('eprosartan','eprosartan','self'),
                  ('eprosartan','teveten',None),
                  ('irbesartan','irbesartan','self'),
                  ('irbesartan','avapro',None),
                  ('losartan','losartan','self'),
                  ('losartan','cozaar',None),
                  ('olmesartan','olmesartan','self'),
                  ('olmesartan','benicar',None),
                  ('telmisartan','telmisartan','self'),
                  ('telmisartan','micardis',None),
                  ('valsartan','valsartan','self'),
                  ('valsartan','diovan',None),
                  ('clonidine','clonidine','self'),
                  ('clonidine','catapres',None),
                  ('doxazosin','doxazosin','self'),
                  ('doxazosin','cardura',None),
                  ('guanfacine','guanfacine','self'),
                  ('guanfacine','tenex',None),
                  ('methyldopa','methyldopa','self'),
                  ('methyldopa','aldomet',None),
                  ('prazosin','prazosin','self'),
                  ('prazosin','minipress',None),
                  ('terazosin','terazosin','self'),
                  ('terazosin','hytrin',None),
                  ('eplerenone','eplerenone','self'),
                  ('eplerenone','inspra',None),
                  ('sprinolactone','sprinolactone','self'),
                  ('sprinolactone','aldactone',None),
                  ('aliskiren','aliskiren','self'),
                  ('aliskiren','tekturna',None),
                  ('hydralazine','hydralazine','self'),
                  ('hydralazine','apresoline',None))
        for syntup in druglist:
               drugrow, created = DrugSynonym.objects.get_or_create(generic_name=syntup[0],other_name=syntup[1],comment=syntup[2])
               if created:
                   drugrow.save()
                   
    @property
    def event_heuristics(self):
        
        # - removed update_drugsyn - conlicts with static migrtion 0011
        # self.update_drugsyn()
        
        heuristic_list = []
        #
        # high blood pressure
        #
        '''heuristic_list.append( EncounterBPHeuristic(
            name = 'bp'
            ))
        #
        # hypertension via sql
        #
        heuristic_list.append( HypertensionDxHeuristic(
            name = 'hypertension'
            ))
        #
        # Diagnosis Codes
        #
        heuristic_list.append( PregnancyDxHeuristic(
            name = 'pregnancy'
            ))
            
        heuristic_list.append( esrDxHeuristic(
            name = 'endstagerenal'
            ))
        '''
        #
        # Prescriptions
        #
        heuristic_list.append( PrescriptionHeuristic(
            name = 'hydrochlorothiazide',
            drugs = DrugSynonym.generics_plus_synonyms(['Hydrochlorothiazide', ]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'chlorthalidone',
            drugs = DrugSynonym.generics_plus_synonyms(['Chlorthalidone' ]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'indapamide',
            drugs =  DrugSynonym.generics_plus_synonyms(['Indapamide']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'amlodipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Amlodipine',]),
            ))    
        heuristic_list.append( PrescriptionHeuristic(
            name = 'clevidipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Clevidipine',]),
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'felodipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Felodipine',]),
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'isradipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Isradipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nicardipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nicardipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nifedipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nifedipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nisoldipine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nisoldipine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'diltiazem',
            drugs =  DrugSynonym.generics_plus_synonyms(['Diltiazem',]),
            ))       
        heuristic_list.append( PrescriptionHeuristic(
            name = 'verapamil',
            drugs =  DrugSynonym.generics_plus_synonyms(['Verapamil',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'acebutolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Acebutolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'atenolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Atenolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'betaxolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Betaxolol']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'bisoprolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Bisoprolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'carvedilol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Carvedilol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'labetolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Labetolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'metoprolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Metoprolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nadolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nadolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'nebivolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Nebivolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'pindolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Pindolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'propranolol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Propranolol',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'benazepril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Benazepril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'catopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Catopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'enalapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Enalapril']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fosinopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Fosinopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'lisinopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Lisinopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'moexipril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Moexipril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'perindopril',
            drugs =  DrugSynonym.generics_plus_synonyms(['perindopril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'quinapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Quinapril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'ramipril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Ramipril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'trandolapril',
            drugs =  DrugSynonym.generics_plus_synonyms(['Trandolapril',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'candesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Candesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'eprosartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Eprosartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'irbesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Irbesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'losartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Losartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'olmesartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Olmesartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'telmisartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Telmisartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'valsartan',
            drugs =  DrugSynonym.generics_plus_synonyms(['Valsartan',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'clonidine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Clonidine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'doxazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Doxazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'guanfacine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Guanfacine',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'methyldopa',
            drugs =  DrugSynonym.generics_plus_synonyms(['Methyldopa',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'prazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Prazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'terazosin',
            drugs =  DrugSynonym.generics_plus_synonyms(['Terazosin',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'eplerenone',
            drugs =  DrugSynonym.generics_plus_synonyms(['Eplerenone',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'sprinolactone',
            drugs =  DrugSynonym.generics_plus_synonyms(['Sprinolactone',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'aliskiren',
            drugs =  DrugSynonym.generics_plus_synonyms(['Aliskiren',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'hydralazine',
            drugs =  DrugSynonym.generics_plus_synonyms(['Hydralazine',]),
            ))
        return heuristic_list
        
    def generate(self):
        #check for various gen_pop_tools infrastructure.  We're just checking to see that the tables exists, we assume if it exists, the remaining infrastructure has been set up
        check1_sql = "select count(*) from gen_pop_tools.clin_enc"
        check2_sql = "select count(*) from gen_pop_tools.rs_conf_mapping"
        cur = connection.cursor()
        try:
            cur.execute(check1_sql)
        except:
            log.error('The clin_enc infrastructure is required for this plugin.  Please see the ESP_tools repository make_clin_enc_tools script and related')
            cur.close()
            sys.exit()
        try:
            cur.execute(check2_sql)
        except:
            log.error('The rs_conf_mapping infrastructure is required for this plugin.  Please see the ESP_tools repository trimtracker/make_rs_conf_mapping script and related')
            cur.close()
            sys.exit()        
        cur.close()
        
        log.info('Generating cases of %s' % self.short_name)
        counter = generate_diagnosed_hypertension_cases()
        return counter # Count of new cases
    
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

def event_heuristics():
    return DiagnosedHypertension().event_heuristics

def disease_definitions():
    return [DiagnosedHypertension()]
